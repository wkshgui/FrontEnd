/*
 * 循环轮播图动画
 */

var self;
var loopSlide = function(container, opts) {
    self = this;
    this.container = document.getElementById(container);
    this.options = {
        width: 400,
        height: 400,
        speed: 3000,
        dely: 2000,
        showIcon: true,  // 底部圆点图标
        pnIcon: true,    // 前进后退按钮
    },
    this.index = 1;
    this.animated = false;
    this.timer;
    this.init(opts);
};
loopSlide.prototype = {
    init: function(opts) {
        console.log(this);
        var that = this;
        
        that.Extend(opts);
        that.container.setAttribute('style',
            'width:' + that.options.width + 'px;'
            + 'height:' + that.options.height + 'px;'
        );

        var items = that.container.getElementsByClassName('items')[0],
            item = items.getElementsByClassName('item'),
            len = item.length;

        items.setAttribute('style',
            'width:'+Math.round(that.options.width*len)+'px;'
            +'height:'+that.options.height+'px;'
            +'left: -'+that.options.width+'px;');

        for (var i = 0; i < item.length; i++) {
            item[i].setAttribute('style',
            'width:'+that.options.width+'px;'
            +'height:'+that.options.height+'px;');

            var img = item[i].getElementsByTagName("img")[0];
            that.setImageSize(img.src, function(w, h){
                that.adaptionWidth(img, {
                    width: w,
                    height: h,
                });
            });
        }

        if (that.options.showIcon) {
            that.setBmIcon();
        }

        if(that.options.pnIcon) {
            that.setPnIcon();
        }

        that.addEvt();
        that.play();
    },

    Extend: function(options) {
        for(var property in options){
            this.options[property] = options[property];
        }   
    },

    setImageSize: function(url,callback){
        var img = new Image();
        img.src = url;
        // 如果图片被缓存，则直接返回缓存数据
        if(img.complete){
          callback(img.width, img.height);
        }else{
            // 完全加载完毕的事件
            img.onload = function(){
                callback(img.width, img.height);
            }
        }
    },

    // 自适应屏宽函数
    adaptionWidth: function (obj, params){
        var that = this;
        var pWidth = that.options.width,
            pHeight = that.options.height,
            rate = params.height / pHeight,
            oWidth = Math.round(params.width / rate);

        if (oWidth > pWidth) {
            var disWidth = oWidth - pWidth;
            obj.setAttribute('style',
                'width:'+oWidth+'px;'
                +'height:'+pHeight+'px;'
                +'margin-left:-'+(disWidth/2)+'px;');
        }else{
            obj.setAttribute('style',
                'width:'+oWidth+'px;'
                +'height:'+pHeight+'px;'
                +'display:block;margin:auto;');
        }
    },

    // 设置底部圆点
    setBmIcon: function(){
        var that = this,
            buttons = document.createElement('div'),
            len = that.container.getElementsByClassName('items')[0].getElementsByClassName('item').length;
        
        buttons.className='buttons';
        for (var i = 0; i < (len-2); i++) {
            var span = document.createElement('span');
            span.setAttribute('index', i+1);
            if (i == 0) {
                span.className='on';
            }
            buttons.appendChild(span);
        }
        that.container.appendChild(buttons);
    },

    // 设置向前向后按钮
    setPnIcon: function(){
        var that = this,
            p_a = document.createElement('a'),
            n_a = document.createElement('a');

        p_a.className = 'J_prev prev arrow';
        p_a.href = 'javascript:void(0);';
        p_a.innerHTML = '<';
        n_a.className = 'J_next next arrow';
        n_a.href = 'javascript:void(0);';
        n_a.innerHTML = '>';

        that.container.appendChild(p_a);
        that.container.appendChild(n_a);
    },

    // 监听事件
    addEvt: function(){
        var that = this,
            button_con = that.container.getElementsByClassName('buttons')[0],
            buttons = button_con && button_con.children,
            prev = that.container.getElementsByClassName('J_prev')[0],
            next = that.container.getElementsByClassName('J_next')[0];

        that.container.onmouseover = that.stop;
        that.container.onmouseout = that.play;

        //点击圆点按钮 偏移
        for (var i = 0; i < buttons.length; i++) {
            buttons[i].onclick = function () {
                if (this.className == "on") {
                    return;
                }
                //要点击的index属性的值 转换为整数
                var myIndex = parseInt(this.getAttribute("index"));
                //偏移量=-that.options.width*（要点击的位置-当前所在的位置），当前的位置就是index循环所得
                var os = -that.options.width * (myIndex - that.index);
                //切换完成后，把点击的index位置变成当前的index位置 
                that.index = myIndex;
                that.showButton();
                if (!that.animated) {
                    that.animate(os);
                }
            }
        };

        //执行所有函数
        next.onclick = function () {
            if (that.index == 5) {
                that.index = 1;
            } else {
                that.index += 1;
            }
            that.showButton();
            if (!that.animated) {
                that.animate(-that.options.width);
            }
        };

        //执行所有函数
        prev.onclick = function () {
            if (that.index == 1) {
                that.index = 5;
            } else {
                that.index -= 1;
            }
            that.showButton();
            if (!that.animated) {
                that.animate(that.options.width);
            }
        };
    },
     
    //当前图片轮播的圆点变色显示，原理：index数值是跟随list滑动次数递增的，第一次index=1，所以第一个button的索引值就是0。
    //for循环是添加on样式之前要清空之前的on。
    showButton: function() {
        var that = this,
            button_con = that.container.getElementsByClassName('buttons')[0],
            buttons = button_con && button_con.children;

        for (var i = 0; i < buttons.length; i++) {
            if (buttons[i].className == "on") {
                buttons[i].className = "";
                break;
            }
        }
        buttons[that.index - 1].className = "on";
    },

    //圆点变色显示 结束。
    animate: function(offset) {
        var that = self;
        that.animated = true;
        var items = that.container.getElementsByClassName('items')[0],
            newLeft = parseInt(items.style.left) + offset;//当前的偏移量+下一次的偏移量=新的偏移量

        var time = 300;//位移总时间
        var interval = 10;//位移间隔时间
        //动画效果自定义公式： 每次位移的距离=单次偏移距离/位移次数
        var speed = offset / (time / interval);
        //递归函数 直到不满足条件（跳到辅助图）
        //递归就是把600偏移量分为多次完成偏移
        function go() {
            //speed<0 并且 当前偏移量>下一次偏移量 就是向左偏移 || 反之向右偏移 
            if ((speed < 0 && parseInt(items.style.left) > newLeft) || (speed > 0 && parseInt(items.style.left) < newLeft)) {
                items.style.left = parseInt(items.style.left) + speed + "px";//每次位移的值
                setTimeout(go, interval);//10毫秒再次调用go函数
            } else {
                that.animated = false;
                items.style.left = newLeft + "px";//当前的偏移值=新的偏移值
                if (newLeft > -that.options.width) {
                    items.style.left = -Math.round(that.options.width * 5) + "px";
                }
                if (newLeft < -Math.round(that.options.width * 5)) {
                    items.style.left = -that.options.width + "px";
                }
            }
        }
        go();
    },

    //自动播放3秒执行一次next.onclick
    play: function() {
        var that = self,
            next = that.container.getElementsByClassName('J_next')[0];
        
        clearInterval(that.timer);
        that.timer = setInterval(function () {
             next.onclick();
        }, 3000);
    },

    stop: function() {
        var that = self;
        clearInterval(that.timer);
    },
};
