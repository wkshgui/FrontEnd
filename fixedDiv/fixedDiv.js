;(function(factory) {
	if (typeof define === 'function' && define.amd) {
		// AMD
		define(['jquery'], factory);
	} else if (typeof module === 'object' && module.exports) {
		factory(require('jquery'));
	} else {
		factory(jQuery);
	}
}(function($) {
	$.fn.fixedDiv = function(obj_pos, scroll_container){
	    var $that = $(this),
	        scroll_container = scroll_container || $that.closest('.grid-container');  // 滚动区对象
	    var pos = 0,          // 当前document所滚动到的top位置
	        btmVal = 0,       // 浏览器可视窗口滚动条底部与document底部的相对差值
	        posLeft,          // 滚动窗口的left值
	        lastLeft = 0,     // 滚动模块最终的left值
	        topVal,           // 初始化时滚动条所在的top值
	        winHeight,        // 浏览器可视窗口的高
	        winWidth,         // 浏览器可视窗口的宽
	        bottomVal = scroll_container.offset().top + scroll_container.height(),   // 滚动区底部相对于document的位置
	        pLeft = $that.parent().offset().left,   // 父容器的left值
	        objHeight = $that.height(),             // 悬浮模块的高
	        objWidth = $that.width(),               // 悬浮模块的宽
	        pRightHeight = 0,                       // 滚动区右侧高度
	        pLeftHeight = 0;                        // 滚动区左侧高度

	    if (obj_pos == "right") {                   // obj_pos悬浮块所在的位置（左、右）
	        pRightHeight = $that.parent("div").height();
	        pLeftHeight = $that.parent("div").prev("div").height();
	    }

	    if (obj_pos == "left") {
	        pRightHeight = $that.parent("div").height();
	        pLeftHeight = $that.parent("div").next("div").height();
	    }

	    if($that.length > 0){
	        topVal = $that.offset().top;
	    }
	    function fix(){
	        winHeight = $(window).height();
	        winWidth = $(window).width();
	        pos = $(document).scrollTop();
	        posLeft = $(document).scrollLeft();
	        pLeft = $that.parent("div").offset().left;

	        // 左侧高度大于右侧时，返回
	        if (pRightHeight >= pLeftHeight) {
	            return;
	        }

	        if (pos > topVal) {
	            bottomVal = scroll_container.offset().top + scroll_container.height();
	            lastLeft = winWidth>1180?pLeft:-(posLeft-pLeft);
	            if (parseInt(pos + objHeight + 20)<bottomVal) {
	                $that.css({
	                    position:'fixed',
	                    top: 0,
	                    left: lastLeft,
	                    width: objWidth,
	                    bottom:'auto'
	                });
	            }else{
	                btmVal = parseInt(winHeight - (bottomVal - pos));
	                $that.css({
	                    position:'fixed',
	                    top:'auto',
	                    left: lastLeft,
	                    bottom: btmVal,
	                    width: objWidth
	                });
	                if (!window.XMLHttpRequest) {
	                    $that.css({
	                        position: 'absolute',
	                        top     : 'auto',
	                        left    : lastLeft,
	                        bottom: btmVal,
	                        width   : objWidth
	                    });
	                }
	            }
	            if (!window.XMLHttpRequest) {
	                $that.css({
	                    position: 'absolute',
	                    top     : pos,
	                    left    : lastLeft,
	                    width   : objWidth
	                });
	            }
	        } else {
	            $that.css({
	                    position: 'static',
	                    top     : 'auto',
	                });
	            if (!window.XMLHttpRequest) {
	                $that.css({
	                    position: 'static',
	                    top     : 'auto',
	                });
	            }
	        }
	    }
	    fix();

	    $(window).scroll(fix);
	    $(window).resize(function(){
	        pLeft = $that.parent("div").offset().left;
	        fix();
	    });
	};
}));